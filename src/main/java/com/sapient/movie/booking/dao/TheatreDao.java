package com.sapient.movie.booking.dao;

import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.entity.Theatre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TheatreDao extends BaseDao<Theatre> {

    Theatre findByName(String theatreName);
}
