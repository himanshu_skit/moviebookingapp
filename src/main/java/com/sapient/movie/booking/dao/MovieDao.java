package com.sapient.movie.booking.dao;

import com.sapient.movie.booking.entity.Movie;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieDao extends BaseDao<Movie> {
    Movie findByName(String name);
}


