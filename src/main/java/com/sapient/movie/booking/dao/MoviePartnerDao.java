package com.sapient.movie.booking.dao;

import com.sapient.movie.booking.entity.Movie;
import com.sapient.movie.booking.entity.MoviePartner;
import org.springframework.stereotype.Repository;

@Repository
public interface MoviePartnerDao extends BaseDao<MoviePartner> {

}
