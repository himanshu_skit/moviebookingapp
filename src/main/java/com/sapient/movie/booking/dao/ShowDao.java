package com.sapient.movie.booking.dao;

import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.entity.Show;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ShowDao extends BaseDao<Show> {

    List<Show> findByMovieId(Long movieId);

    @Query(
            value = "SELECT \n" +
                    "  SHOW.name as showname, \n" +
                    "  show.start_time as startTime, \n" +
                    "  show.end_time as endTime, \n" +
                    "  THEATRE.name as theatreName, \n" +
                    "  SITE.name as siteName, \n" +
                    "  site.city, \n" +
                    "  site.address, \n" +
                    "  movies.name as movieName \n" +
                    "FROM \n" +
                    "  SHOW \n" +
                    "  join THEATRE on SHOW.theatre_id = THEATRE.id \n" +
                    "  join movies on SHOW.movie_id = movies.id \n" +
                    "  join site on THEATRE.site_id = SITE.id \n" +
                    "where \n" +
                    "  movies.name = :movie \n" +
                    "  and site.city = :city \n" +
                    "  and FORMATDATETIME(show.start_time, 'yyyy-MM-dd') = :localDate",
            nativeQuery = true)
    List<ShowDetails> findShowsByCityMovieDate(@Param("city") String city, @Param("movie") String movie, @Param("localDate") LocalDate localDate);


}





