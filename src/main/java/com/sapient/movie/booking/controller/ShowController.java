package com.sapient.movie.booking.controller;

import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.service.ShowService;
import com.sapient.movie.booking.service.TheatreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/shows")
public class ShowController {

    @Autowired
    private ShowService showService;


    //Eg: GET /shows?city=Noida&movie=Openheimer&date=01-08-2023
    @GetMapping
    ResponseEntity<Object> getShowsByCityMovieDate(@RequestParam(value = "city") final String city,
                                                      @RequestParam(value = "movie") final String movie,
                                                      @RequestParam(value = "date") final String date) {


        List<ShowDetails> shows = showService.findShowsByCityMovieDate(city, movie, date);
        return ResponseEntity.ok(shows);
    }

}
