package com.sapient.movie.booking.controller;

import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.entity.Theatre;
import com.sapient.movie.booking.service.TheatreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/theatres")
public class TheatreController {

    @Autowired
    private TheatreService theatreService;

    @GetMapping
    ResponseEntity<Object> getAll() {

        List<Theatre> theatres = theatreService.findAll();
        return ResponseEntity.ok(theatres);
    }
}
