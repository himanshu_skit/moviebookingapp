package com.sapient.movie.booking.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieBookingException extends RuntimeException {

    private int errorCode;
    private HttpStatus httpStatus;
    private String message;


    public MovieBookingException(String message) {
        super(message);
        this.message = message;
    }
}
