package com.sapient.movie.booking.exception;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class MovieBookingExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(MovieBookingException.class)
    public final ResponseEntity<Object> handleExceptions(MovieBookingException ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(
                ex.getHttpStatus().value(),
                ex.getHttpStatus());
        return new ResponseEntity<>(error, ex.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("httpCode", status.value());
        body.put("httpStatus", HttpStatus.BAD_REQUEST);
        body.put("appErrorMessage", ex.getBindingResult().getFieldError().getDefaultMessage());
        body.put("appErrorCode", 0);
        return new ResponseEntity<>(body, headers, status);
    }
}
