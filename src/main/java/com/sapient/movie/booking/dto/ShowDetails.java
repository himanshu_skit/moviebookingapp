package com.sapient.movie.booking.dto;

import java.time.LocalDateTime;


public interface ShowDetails {
    String getShowname();
    String getTheatreName();
    String getSiteName();
    String getCity();
    String getAddress();
    LocalDateTime getStartTime();
    LocalDateTime getEndTime();
    String getMovieName();

}

