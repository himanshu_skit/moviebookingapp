package com.sapient.movie.booking.entity;

public enum SeatType {
    RECLINER,
    NORMAL,
    UNUSED
}
