package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;


@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "movies")
public class Movie extends BaseEntity {
    private String name;
    private String genres;
    private String language;

}
