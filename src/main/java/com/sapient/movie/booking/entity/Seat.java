package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "seat")
public class Seat extends BaseEntity {
    private Integer rowNum;

    private Integer columnNum;

    private SeatType type = SeatType.NORMAL;

    @ManyToOne
    private Theatre theatre;

    public Seat(int rowNum, int columnNum, SeatType seatType) {

        this.rowNum = rowNum;
        this.columnNum = columnNum;
        this.type = seatType;

    }
}

