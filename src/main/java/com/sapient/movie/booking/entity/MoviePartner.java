package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "partners")
public class MoviePartner extends BaseEntity {

    private String name;

    @Lob
    private byte[] image;

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

    @OneToMany(mappedBy = "moviePartner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Site> sites = new ArrayList<>();

    public void addSite(Site site) {
        sites.add(site);
        site.setMoviePartner(this);
    }

    public void removeSite(Site site) {
        sites.remove(site);
        site.setMoviePartner(null);
    }


}

