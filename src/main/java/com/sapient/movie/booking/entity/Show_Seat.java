package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "show_seat")
public class Show_Seat extends BaseEntity {

   private Double price;

   private SeatStatus status = SeatStatus.AVAILABLE;

   @OneToOne
   private Seat seat;

   @OneToOne
   private Show show;
}

