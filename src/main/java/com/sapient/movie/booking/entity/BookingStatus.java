package com.sapient.movie.booking.entity;

public enum BookingStatus {
    PENDING,
    CONFIRMED,
    CANCELLED

}
