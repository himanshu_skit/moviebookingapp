package com.sapient.movie.booking.entity;

public enum SeatStatus {
    BOOKED,
    AVAILABLE,
    RESERVED,
    BLOCKED

}


