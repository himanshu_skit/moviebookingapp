package com.sapient.movie.booking.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "theatre")
public class Theatre extends BaseEntity {

    private String name;

    @Lob
    @JsonIgnore
    private byte[] image;

    @OneToMany(mappedBy = "theatre", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Seat> seats = new ArrayList<>();

    @ManyToOne()
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "city")
    @JsonIdentityReference(alwaysAsId = true)
    private Site site;

    public Theatre(String name) {
        this.name = name;
    }

   public void add(List<Seat> seats) {
        for (Seat seat : seats) {
            add(seat);
        }
    }

    public void remove(List<Seat> seats) {
        for (Seat seat : seats) {
            remove(seat);
        }
    }

    public void add(Seat seat) {
        seats.add(seat);
        seat.setTheatre(this);
    }

    public void remove(Seat seat) {
        seats.remove(seat);
        seat.setTheatre(null);
    }
}



