package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "booking")
public class Booking extends BaseEntity {

    private String name;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Integer capacity;

    @OneToOne()
    private Theatre theatre;

    @OneToOne()
    private Movie movie;

    @OneToOne()
    private Site site;

    @OneToOne()
    private MoviePartner moviePartner;

    @OneToOne()
    private User user;
}



