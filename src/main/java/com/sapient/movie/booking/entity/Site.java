package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "site")
public class Site extends BaseEntity {

    private String name;

    private String address;

    private String city;

    @Lob
    private byte[] image;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Theatre> theatres = new ArrayList<>();

    @ManyToOne()
    private MoviePartner moviePartner;

    public Site(String name, String address, String city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public void addTheatre(List<Theatre> theatres) {
      for (Theatre theater : theatres) {
          addTheatre(theater);
      }
    }

    public void removeTheatre(List<Theatre> theatres) {
        for (Theatre theater : theatres) {
            removeTheatre(theater);
        }
    }

    public void addTheatre(Theatre theatre) {
        theatres.add(theatre);
        theatre.setSite(this);
    }

    public void removeTheatre(Theatre theatre) {
        theatres.remove(theatre);
        theatre.setSite(null);
    }

}



