package com.sapient.movie.booking.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User extends BaseEntity {

    private String name;
    private LocalDateTime dob;
    private String email;
    private String phone;


}



