package com.sapient.movie.booking.service;

import com.sapient.movie.booking.dao.BaseDao;
import com.sapient.movie.booking.dao.MovieDao;
import com.sapient.movie.booking.dao.ShowDao;
import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.entity.Movie;
import com.sapient.movie.booking.entity.Show;
import com.sapient.movie.booking.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;


@Service
public class ShowService extends BaseService<Show> {

    @Autowired
    private ShowDao showDao;

    @Autowired
    private MovieDao movieDao;

    public ShowService(BaseDao<Show> dao) {
        super(dao);
    }

    public List<ShowDetails> findShowsByCityMovieDate(String city, String movie, String date) {

        LocalDate startDateObj = DateTimeUtil.getLocalDateFromString(date);
        return showDao.findShowsByCityMovieDate(city, movie, startDateObj);

    }
}
