package com.sapient.movie.booking.service;

import com.sapient.movie.booking.dao.BaseDao;
import com.sapient.movie.booking.dao.MovieDao;
import com.sapient.movie.booking.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MovieService extends BaseService<Movie> {

    @Autowired
    private MovieDao movieDao;

    public MovieService(BaseDao<Movie> dao) {
        super(dao);
    }


    public Movie findByName(String name) {
        return movieDao.findByName(name);
    }
}
