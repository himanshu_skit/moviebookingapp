package com.sapient.movie.booking.service;

import com.sapient.movie.booking.dao.BaseDao;
import com.sapient.movie.booking.dao.TheatreDao;
import com.sapient.movie.booking.dto.ShowDetails;
import com.sapient.movie.booking.entity.Theatre;
import com.sapient.movie.booking.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TheatreService extends BaseService<Theatre> {

    @Autowired
    private TheatreDao theatreDao;

    public TheatreService(BaseDao<Theatre> dao) {
        super(dao);
    }

    public Theatre findByName(String theatreName) {
        return theatreDao.findByName(theatreName);
    }
}


