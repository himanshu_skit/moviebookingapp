package com.sapient.movie.booking.service;

import com.sapient.movie.booking.dao.BaseDao;
import com.sapient.movie.booking.entity.Movie;
import com.sapient.movie.booking.entity.MoviePartner;
import org.springframework.stereotype.Service;

@Service
public class MoviePartnerService extends BaseService<MoviePartner> {

    public MoviePartnerService(BaseDao<MoviePartner> dao) {
        super(dao);
    }
}
