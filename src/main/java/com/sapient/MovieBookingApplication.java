package com.sapient;

import com.sapient.movie.booking.entity.*;
import com.sapient.movie.booking.service.MoviePartnerService;
import com.sapient.movie.booking.service.MovieService;
import com.sapient.movie.booking.service.ShowService;
import com.sapient.movie.booking.service.TheatreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class MovieBookingApplication implements CommandLineRunner {

    public static final String PVR_NOIDA_EMERALD = "PVR-Noida Extension Emerald Theatre";
    public static final String PVR_NOIDA_EUROPA = "PVR-Noida Extension Europa Theatre";
    public static final String PVR_NOIDA_GALAXY = "PVR-Noida Extension Galaxy Theatre";
    public static final String PVR_GGM_EMERALD = "PVR-Gurugram Emerald Theatre";
    public static final String PVR_GGM_EUROPA = "  PVR-Gurugram Europa Theatre";

    public static final String PARTNER_PVR = "PVR";
    public static final String MOVIE_OPENHEIMER = "Openheimer";
    public static final String LANG_ENGLISH = "English";
    public static final String LANG_HINDI = "Hindi";
    public static final String MOVIE_GODFATHER = "The Godfather";
    public static final String MOVIE_AVENGER = "Avenger1";
    public static final String MOVIE_SHAMSHERA = "Shamshera";
    public static final String GENRE_DRAMA = "DRAMA";

    @Autowired
    MovieService movieService;

    @Autowired
    TheatreService theatreService;

    @Autowired
    MoviePartnerService moviePartnerService;

    @Autowired
    ShowService showService;

    public static void main(String[] args) {
        SpringApplication.run(MovieBookingApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        setupMovies();

        setupPVRMoviePartner();

        setupShow("Morning Show - Openheimer", LocalDateTime.of(
                2023, Month.AUGUST, 01, 9, 00), MOVIE_OPENHEIMER, PVR_NOIDA_EMERALD);

        setupShow("Matinee Show - Openheimer", LocalDateTime.of(
                2023, Month.AUGUST, 01, 14, 00), MOVIE_OPENHEIMER, PVR_NOIDA_EMERALD);

        setupShow("Matinee Show - Openheimer", LocalDateTime.of(
                2023, Month.AUGUST, 01, 13, 00), MOVIE_OPENHEIMER, PVR_NOIDA_EUROPA);

        setupShow("Morning Show - Openheimer", LocalDateTime.of(
                2023, Month.AUGUST, 01, 10, 00), MOVIE_OPENHEIMER, PVR_GGM_EMERALD);

    }

    private void setupShow(String showName, LocalDateTime showStartTime, String movieName, String theatreName) {

        Show show = new Show();
        show.setName(showName);
        show.setStartTime(showStartTime);
        show.setEndTime(showStartTime.plusHours(3));
        show.setMovie(movieService.findByName(movieName));
        show.setTheatre(theatreService.findByName(theatreName));
        show.setCapacity(100);
        showService.save(show);
    }

    private void setupPVRMoviePartner() {
        MoviePartner moviePartner = new MoviePartner();
        moviePartner.setName(PARTNER_PVR);

        Site siteNoidaExtension = createNoidaExtensionSite("PVR-Noida Extension", "Noida Extension", "Noida");
        Site siteGurugram = createGurugramSite("PVR-Gurugram", "GGM", "Gurugram");

        moviePartner.addSite(siteNoidaExtension);
        moviePartner.addSite(siteGurugram);

        moviePartnerService.save(moviePartner);
    }

    private Site createNoidaExtensionSite(String name, String address, String city) {
        Site site = new Site(name, address, city);

        Theatre theatreEmerald = createTheatre(PVR_NOIDA_EMERALD);
        Theatre theatreEuropa = createTheatre(PVR_NOIDA_EUROPA);
        Theatre theatreGalaxy = createTheatre(PVR_NOIDA_GALAXY);

        List<Theatre> theatres = Arrays.asList(theatreEmerald, theatreEuropa, theatreGalaxy);
        site.addTheatre(theatres);
        return site;
    }

    private Site createGurugramSite(String name, String address, String city) {
        Site site = new Site(name, address, city);

        Theatre theatreEmerald = createTheatre(PVR_GGM_EMERALD);
        Theatre theatreEuropa = createTheatre(PVR_GGM_EUROPA);
        Theatre theatreGalaxy = createTheatre("PVR-Gurugram Extension Galaxy Theatre");

        List<Theatre> theatres = Arrays.asList(theatreEmerald, theatreEuropa, theatreGalaxy);
        site.addTheatre(theatres);
        return site;
    }

    private Theatre createTheatre(String name) {
        Theatre theatre = new Theatre(name);
        Seat row1Seat1 = new Seat(0, 0, SeatType.NORMAL);
        Seat row1Seat2 = new Seat(0, 1, SeatType.NORMAL);
        Seat row1Seat3 = new Seat(0, 2, SeatType.NORMAL);

        Seat row2Seat1 = new Seat(1, 0, SeatType.NORMAL);
        Seat row2Seat2 = new Seat(1, 1, SeatType.NORMAL);
        Seat row2Seat3 = new Seat(1, 2, SeatType.NORMAL);

        Seat row3Seat1 = new Seat(2, 0, SeatType.NORMAL);
        Seat row3Seat2 = new Seat(2, 1, SeatType.NORMAL);
        Seat row3Seat3 = new Seat(2, 2, SeatType.NORMAL);

        //create list of seats
        List<Seat> seats = Arrays.asList(row1Seat1, row1Seat2, row1Seat3, row2Seat1, row2Seat2, row2Seat3, row3Seat1, row3Seat2, row3Seat3);
        theatre.add(seats);
        return theatre;
    }


    private void setupMovies() {

        Movie movie = new Movie(MOVIE_GODFATHER, GENRE_DRAMA, LANG_ENGLISH);
        Movie movie2 = new Movie(MOVIE_AVENGER, "ACTION", LANG_ENGLISH);
        Movie movie3 = new Movie(MOVIE_OPENHEIMER, GENRE_DRAMA, LANG_ENGLISH);
        Movie movie4 = new Movie(MOVIE_SHAMSHERA, GENRE_DRAMA, LANG_HINDI);

        List<Movie> movies = Arrays.asList(movie, movie2, movie3, movie4);
        movieService.saveAll(movies);

    }

}
