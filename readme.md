# For Running the app on local

## Step 1: run the below command inside the project folder:

```
mvn clean install
```
## It will generate a target folder with application jar in it. Execute the application with the below command:
```
java -jar target/MovieBookingApp-0.0.1-SNAPSHOT.jar
```

## Step 2: Run the below command to tag and build the docker image

```
docker build -t gcr.io/<gke-project-id>/<app_name> .
```
e.g. 
```
docker build -t gcr.io/<gke-project-id>/movie-booking-app:v1 .
```

Don't miss the dot in the end of above mentioned command.

## Step 3: To run the app on your local docker instance execute below command
```
docker run -p 8080:8080 gcr.io/<gke-project-id>/movie-booking-app:v1
```

## Step 4: Now you can access the app on your browser or rest client on localhost:8080. Sample endpoint is 
```
GET: localhost:8080/theatres
GET: localhost:8080/shows?city=Noida&movie=Openheimer&date=01-08-2023
```

# For Running the app on kubernetes After the step 1 and step 2 execute below steps

## Step 5: Push your image to Google Container Registry by running below command
```
docker push gcr.io/<gke-project-id>/movie-booking-app:v1
```

## Step 6: Login into GCP and create a kubernetes cluster with the name "movie-booking-cluster" or any other name of your choice

## Step 7: Open kube/deployment.yaml file which is included with the codebase and change the placeholder <your_Google_project_id> with your Google Project Id. 

## Step 8: Run below commands on gcp cloud shell to configure project and compute zone 

```
gcloud config set project <your_project_name>
gcloud config set compute/zone <your_compute_zone>
```

## Step 9: Fetch the credentials using below command:
```
gcloud container clusters get-credentials movie-booking-cluster
```

## Step 10: run below command in gcp cloud shell to deploy the service.
```
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

```

## Step 11: Run Below command to get the external IP. (If external ip is not visible, wait for few seconds and run command again)

```
kubectl get svc
```

