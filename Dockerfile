#FROM openjdk:8-jdk-alpine
FROM openjdk:17-alpine
MAINTAINER himanshu.agrawal
COPY target/MovieBookingApp-0.0.1-SNAPSHOT.jar MovieBookingApp-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/MovieBookingApp-0.0.1-SNAPSHOT.jar"]